﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Logistics.Model;

namespace Logistics.DAL
{
    public class LogisticsRepository : ILogisticsRepository
    {
        static string _connectionString;
        public LogisticsRepository()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string newPath = Path.GetFullPath(Path.Combine(path, @"..\..\..\"));
            string connectionStr = Path.GetFullPath(Path.Combine(newPath, @"ERP\ERP.DAL\Database\Logistics.mdf"));
            _connectionString = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={connectionStr};Integrated Security=True";

            _connectionString = "Server=localhost\\PRESCIENTSQL;Database=Logistics;integrated security=True;MultipleActiveResultSets=True;";
        }
        public bool AddStairsToSite(int stairsId)
        {
            string sql = "Insert into ElementsOnSiteEntity (ElementId, Date) Values (@ElementId, @Date)";

            using (var connection = new SqlConnection(_connectionString))
            {
                var numberOfAffectedRows = connection.Execute(sql,
                    new ElementsOnSiteEntity
                    {
                        ElementId = stairsId,
                        Date = DateTime.Now
                    });
                return numberOfAffectedRows > 1;
            }
        }

        public bool AddStairsToSiteRollback(int stairsId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var numberOfAffectedRows = connection.Execute("DELETE FROM [dbo].[ElementsOnSiteEntity]");
                return numberOfAffectedRows > 1;
            }
        }

        public async Task<IList<ElementsOnSiteEntity>> GetElementsOnSites()
        {
            string sql = "SELECT * FROM ElementsOnSiteEntity";

            using (var connection = new SqlConnection(_connectionString))
            {
                var stairsData = await connection.QueryAsync<ElementsOnSiteEntity>(sql);
                return stairsData.ToList();
            }
        }

    }
}
