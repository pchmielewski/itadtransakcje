﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Logistics.Model;

namespace Logistics.DAL
{
    public interface ILogisticsRepository
    {
        bool AddStairsToSite(int stairsId);
        bool AddStairsToSiteRollback(int stairsId);
        Task<IList<ElementsOnSiteEntity>> GetElementsOnSites();
    }
}
