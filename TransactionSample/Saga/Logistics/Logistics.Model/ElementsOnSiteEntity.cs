﻿using System;

namespace Logistics.Model
{
    public class ElementsOnSiteEntity
    {
        public int Id { get; set; }
        public int ElementId { get; set; }
        public DateTime Date { get; set; }
    }
}
