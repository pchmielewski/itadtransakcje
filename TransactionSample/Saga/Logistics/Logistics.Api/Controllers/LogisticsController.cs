﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logistics.DAL;
using Logistics.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogisticsController: Controller
    {
        private readonly ILogisticsRepository _logisticsRepository;

        public LogisticsController(ILogisticsRepository logisticsRepository)
        {
            _logisticsRepository = logisticsRepository;
        }
        [HttpGet]
        public string Info()
        {
            return "Logistics API";
        }

        [HttpGet]
        [Route("GetElementsOnSites/{elementId}")]
        public async Task<IEnumerable<ElementsOnSiteDto>> GetElementsOnSites(int elementId)
        {
            return (await _logisticsRepository.GetElementsOnSites()).Select(p=> new ElementsOnSiteDto
            {
                Id = p.Id,
                ElementId = p.ElementId,
                Date = p.Date
            });
        }

        [HttpPost]
        [Route("AddStairsToSite")]
        public IActionResult AddStairsToSite([FromBody] int elementId)
        {
            _logisticsRepository.AddStairsToSite(elementId);
            return Ok();
        }

        [HttpPost]
        [Route("AddStairsToSiteRollback")]
        public IActionResult AddStairsToSiteRollback([FromBody] int elementId)
        {
            _logisticsRepository.AddStairsToSiteRollback(elementId);
            return Ok();
        }
    }
}
