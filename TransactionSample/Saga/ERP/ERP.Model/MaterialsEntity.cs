﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Model
{
    public class MaterialsEntity
    {
        public int Id { get; set; }
        public int NumberOfElements { get; set; }
        public string ElementType { get; set; }
    }
}
