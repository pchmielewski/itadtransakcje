﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using ERP.Model;

namespace ERP.DAL
{
    public class ErpRepository : IErpRepository
    {
        static string _connectionString;
        public ErpRepository()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string newPath = Path.GetFullPath(Path.Combine(path, @"..\..\..\"));
            string connectionStr = Path.GetFullPath(Path.Combine(newPath, @"ERP\ERP.DAL\Database\ErpDB.mdf"));
            _connectionString = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={connectionStr};Integrated Security=True";

            _connectionString = "Server=localhost\\PRESCIENTSQL;Database=ERPDB;integrated security=True;MultipleActiveResultSets=True;";
        }
        public bool AddStairsToSite(int stairsId)
        {
            string sql = "Insert into ElementsOnSiteEntity (ElementId, Date) Values (@ElementId, @Date)";

            using (var connection = new SqlConnection(_connectionString))
            {
                var numberOfAffectedRows = connection.Execute(sql,
                    new ElementsOnSiteEntity
                    {
                        ElementId = stairsId,
                        Date = DateTime.Now
                    });
                return numberOfAffectedRows > 1;
            }
        }

        public bool UpdateStairsNumbersInErpSystem(int stairsId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = "SELECT * FROM Materials where ElementType = 'Stairs'";
                var stairsData = connection.QueryFirst<MaterialsEntity>(sql);

                string sqlUpdate = @"UPDATE [dbo].[Materials] 
                SET[NumberOfElements] = @NumberOfElements 
                WHERE ElementType = 'Stairs'";

                int numberOfElements = stairsData.NumberOfElements - 1;

                var numberOfAffectedRows = connection.Execute(sqlUpdate,
                    new MaterialsEntity
                    {
                        ElementType = "Stairs",
                        NumberOfElements = numberOfElements
                    });

                // ROLBACK Scenario
                throw new Exception("Problem with connection to ErpDB");

                return numberOfAffectedRows > 1;
            }
        }

        public bool UpdateStairsNumbersInErpSystemRollback(int stairsId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = "SELECT * FROM Materials where ElementType = 'Stairs'";
                var stairsData = connection.QueryFirst<MaterialsEntity>(sql);

                string sqlUpdate = @"UPDATE [dbo].[Materials] 
                SET[NumberOfElements] = @NumberOfElements 
                WHERE ElementType = 'Stairs'";

                int numberOfElements = stairsData.NumberOfElements + 1;

                var numberOfAffectedRows = connection.Execute(sqlUpdate,
                    new MaterialsEntity
                    {
                        ElementType = "Stairs",
                        NumberOfElements = numberOfElements
                    });
                return numberOfAffectedRows > 1;
            }
        }

        public async Task<IList<ElementsOnSiteEntity>> GetElementsOnSites()
        {
            string sql = "SELECT * FROM ElementsOnSiteEntity";

            using (var connection = new SqlConnection(_connectionString))
            {
                var stairsData = await connection.QueryAsync<ElementsOnSiteEntity>(sql);
                return stairsData.ToList();
            }
        }

        public async Task<IList<MaterialsEntity>> GetMaterialsEntity()
        {
            string sql = "SELECT * FROM Materials";

            using (var connection = new SqlConnection(_connectionString))
            {
                var stairsData = await connection.QueryAsync<MaterialsEntity>(sql);
                return stairsData.ToList();
            }
        }
    }
}
