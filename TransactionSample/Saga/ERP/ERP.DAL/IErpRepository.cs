﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.Model;

namespace ERP.DAL
{
    public interface IErpRepository
    {
        bool UpdateStairsNumbersInErpSystem(int stairsId);
        bool UpdateStairsNumbersInErpSystemRollback(int stairsId);
        Task<IList<MaterialsEntity>> GetMaterialsEntity();
    }
}