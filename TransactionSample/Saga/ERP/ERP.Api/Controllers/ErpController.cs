﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.DAL;
using Logistics.Dto;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ErpController : Controller
    {
        private readonly IErpRepository _erpRepository;

        public ErpController(IErpRepository erpRepository)
        {
            _erpRepository = erpRepository;
        }
        [HttpGet]
        public string Info()
        {
            return "ERP API";
        }

        [HttpGet]
        [Route("GetMaterialsEntity/{elementId}")]
        public async Task<IEnumerable<MaterialsDto>> GetMaterialsEntity(int elementId)
        {
            return (await _erpRepository.GetMaterialsEntity()).Select(p=>new MaterialsDto
            {
                Id = p.Id,
                ElementType = p.ElementType,
                NumberOfElements = p.NumberOfElements
            });
        }

        [HttpPost]
        [Route("UpdateStairsNumbersInErpSystem")]
        public IActionResult UpdateStairsNumbersInErpSystem([FromBody] int elementId)
        {
            _erpRepository.UpdateStairsNumbersInErpSystem(elementId);
            return Ok();
        }

        [HttpPost]
        [Route("UpdateStairsNumbersInErpSystemRollback")]
        public IActionResult UpdateStairsNumbersInErpSystemRollback([FromBody] int elementId)
        {
            _erpRepository.UpdateStairsNumbersInErpSystemRollback(elementId);
            return Ok();
        }
    }
}