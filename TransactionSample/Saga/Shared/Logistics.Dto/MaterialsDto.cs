﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logistics.Dto
{
    public class MaterialsDto
    {
        public int Id { get; set; }
        public int NumberOfElements { get; set; }
        public string ElementType { get; set; }
    }
}
