﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logistics.Dto
{
    public class FactoryElementsDto
    {
        public int Id { get; set; }
        public int ElementId { get; set; }
        public string ElementType { get; set; }
        public string State { get; set; }
    }
}
