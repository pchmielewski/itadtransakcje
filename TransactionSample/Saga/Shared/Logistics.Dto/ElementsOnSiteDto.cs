﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logistics.Dto
{
    public class ElementsOnSiteDto
    {
        public int Id { get; set; }
        public int ElementId { get; set; }
        public DateTime Date { get; set; }
    }
}
