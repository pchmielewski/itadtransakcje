﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BimElements.Model;
using Dapper;

namespace BimElements.DAL
{
    public class BimElementsRepository : IBimElementsRepository
    {
        static string _connectionString;
        public BimElementsRepository()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string newPath = Path.GetFullPath(Path.Combine(path, @"..\..\..\"));
            string connectionStr = Path.GetFullPath(Path.Combine(newPath, @"BimElements/BimElements.DAL/Database/BimElementsDB.mdf"));
            _connectionString = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={connectionStr};Integrated Security=True";

            _connectionString = "Server=localhost\\PRESCIENTSQL;Database=BIMELEMENTSDB;integrated security=True;MultipleActiveResultSets=True;";
        }
        public async Task<IList<StairsEntity>> GetInformationAboutStairs()
        {
            string sql = "SELECT * FROM Stairs";

            using (var connection = new SqlConnection(_connectionString))
            {
                var stairsData = await connection.QueryAsync<StairsEntity>(sql);
                return stairsData.ToList();
            }
        }
    }
}