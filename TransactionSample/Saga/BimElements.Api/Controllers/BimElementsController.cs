﻿using Logistics.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BimElements.DAL;

namespace BimElements.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BimElementsController : Controller
    {
        private readonly IBimElementsRepository bimElementsRepository;

        public BimElementsController(IBimElementsRepository bimElementsRepository)
        {
            this.bimElementsRepository = bimElementsRepository;
        }

        [HttpGet]
        public string Info()
        {
            return "BimElements API";
        }

        [HttpGet]
        [Route("InformationAboutStairs")]
        public async Task<IEnumerable<StairsDto>> GetInformationAboutStairs()
        {
            return (await bimElementsRepository.GetInformationAboutStairs()).Select(p=> new StairsDto
            {
                Id =p.Id,
                Length =p.Length,
                Name =p.Name,
                NumberOfLevels =p.NumberOfLevels,
                NumberOfStairs =p.NumberOfStairs,
                Weight =p.Weight,
                Width =p.Width
            });
        }
    }
}
