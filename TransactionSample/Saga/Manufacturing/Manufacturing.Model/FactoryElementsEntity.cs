﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manufacturing.Model
{
    public class FactoryElementsEntity
    {
        public int Id { get; set; }
        public int ElementId { get; set; }
        public string ElementType { get; set; }
        public string State { get; set; }
    }
}
