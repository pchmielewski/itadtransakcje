﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logistics.Dto;
using Manufacturing.DAL;
using Microsoft.AspNetCore.Mvc;

namespace Manufacturing.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManufacturingController : Controller
    {
        private readonly IManufacturingRepository _manufacturingRepository;

        public ManufacturingController(IManufacturingRepository manufacturingRepository)
        {
            _manufacturingRepository = manufacturingRepository;
        }

        [HttpGet]
        public string Info()
        {
            return "Manufacturing API";
        }

        [HttpGet]
        [Route("GetFactoryElements/{elementId}")]
        public async Task<IEnumerable<FactoryElementsDto>> GetFactoryElements(int elementId)
        {
            return (await _manufacturingRepository.GetFactoryElementsEntity(elementId)).Select(p=>new FactoryElementsDto
            {
                ElementId = p.ElementId,
                Id = p.Id,
                ElementType = p.ElementType,
                State = p.State
            });
        }

        // POST api/values
        [HttpPost]
        [Route("UpdateNumberOfStairsInFactory")]
        public IActionResult UpdateNumberOfStairsInFactory([FromBody] int elementId)
        {
            _manufacturingRepository.UpdateNumberOfStairsInFactory(elementId);
            return Ok();
        }

        [HttpPost]
        [Route("UpdateNumberOfStairsInFactoryRollback")]
        public IActionResult UpdateNumberOfStairsInFactoryRollback([FromBody] int elementId)
        {
            _manufacturingRepository.UpdateNumberOfStairsInFactoryRollback(elementId);
            return Ok();
        }
    }
}
