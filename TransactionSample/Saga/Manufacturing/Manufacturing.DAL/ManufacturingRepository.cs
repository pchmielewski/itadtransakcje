﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Manufacturing.Model;

namespace Manufacturing.DAL
{
    public class ManufacturingRepository : IManufacturingRepository
    {
        static string _connectionString;
        public ManufacturingRepository()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string newPath = Path.GetFullPath(Path.Combine(path, @"..\..\..\"));
            string connectionStr = Path.GetFullPath(Path.Combine(newPath, @"Manufacturing\Manufacturing.DAL\Database\ManufacturingDB.mdf"));
            _connectionString = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={connectionStr};Integrated Security=True";

            _connectionString = "Server=localhost\\PRESCIENTSQL;Database=MANUFACTURINGDB;integrated security=True;MultipleActiveResultSets=True;";
        }
        public bool UpdateNumberOfStairsInFactory(int stairsId)
        {
            string sql = "Insert into FactoryElementsEntity (ElementId, ElementType, State) Values (@ElementId, @ElementType, @State)";

            using (var connection = new SqlConnection(_connectionString))
            {
                var numberOfAffectedRows = connection.Execute(sql,
                    new FactoryElementsEntity
                    {
                        ElementId = stairsId,
                        ElementType = "Stairs",
                        State = "OnSite"
                    });
                return numberOfAffectedRows > 1;
            }
        }

        public bool UpdateNumberOfStairsInFactoryRollback(int stairsId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var numberOfAffectedRows = connection.Execute("DELETE FROM [dbo].[FactoryElementsEntity]");
                return numberOfAffectedRows > 1;
            }
        }

        public async Task<IList<FactoryElementsEntity>> GetFactoryElementsEntity(int stairsId)
        {
            string sql = "SELECT * FROM FactoryElementsEntity";

            using (var connection = new SqlConnection(_connectionString))
            {
                var stairsData = await connection.QueryAsync<FactoryElementsEntity>(sql);
                return stairsData.ToList();
            }
        }
    }
}
