﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Manufacturing.Model;

namespace Manufacturing.DAL
{
    public interface IManufacturingRepository
    {
        bool UpdateNumberOfStairsInFactory(int stairsId);
        bool UpdateNumberOfStairsInFactoryRollback(int stairsId);
        Task<IList<FactoryElementsEntity>> GetFactoryElementsEntity(int stairsId);
    }
}