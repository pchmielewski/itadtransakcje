﻿using RestSharp;
using System;
using System.Net;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace Logistic.App.WorkflowSteps
{
    public class UpdateStairsNumbersInErpSystemStep : StepBody
    {
        private readonly RestClient _erpApi;
        public int ElementId { get; set; }
        public UpdateStairsNumbersInErpSystemStep()
        {
            _erpApi = new RestClient("http://localhost:49885/api/erp");
        }
        public override ExecutionResult Run(IStepExecutionContext context)
        {
            Console.WriteLine("EXECUTED UpdateStairsNumbersInErpSystemStep");

            var request = new RestRequest("UpdateStairsNumbersInErpSystem", DataFormat.Json);
            request.AddJsonBody(ElementId);
            var response = _erpApi.Post(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Problem with api");
            }


            return ExecutionResult.Next();
        }
    }
}
