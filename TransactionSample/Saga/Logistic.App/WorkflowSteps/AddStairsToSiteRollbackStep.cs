﻿using RestSharp;
using System;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace Logistic.App.WorkflowSteps
{
    public class AddStairsToSiteRollbackStep : StepBody
    {
        private readonly RestClient _logisticApi;
        public int ElementId { get; set; }
        public AddStairsToSiteRollbackStep()
        {
            _logisticApi = new RestClient("http://localhost:50099/api/Logistics");
        }
        public override ExecutionResult Run(IStepExecutionContext context)
        {
            Console.WriteLine("EXECUTED AddStairsToSiteRollbackStep");

            var request = new RestRequest("AddStairsToSiteRollback", DataFormat.Json);
            request.AddJsonBody(ElementId);
            var response = _logisticApi.Post(request);

            return ExecutionResult.Next();
        }
    }
}
