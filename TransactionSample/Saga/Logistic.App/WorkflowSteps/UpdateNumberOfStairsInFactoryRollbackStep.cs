﻿using RestSharp;
using System;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace Logistic.App.WorkflowSteps
{
    public class UpdateNumberOfStairsInFactoryRollbackStep : StepBody
    {
        private readonly RestClient _manufacturingApi;
        public int ElementId { get; set; }
        public UpdateNumberOfStairsInFactoryRollbackStep()
        {
            _manufacturingApi = new RestClient("http://localhost:49909/api/Manufacturing/");
        }
        public override ExecutionResult Run(IStepExecutionContext context)
        {
            Console.WriteLine("EXECUTED UpdateNumberOfStairsInFactoryRollbackStep");

            var request = new RestRequest("UpdateNumberOfStairsInFactoryRollback", DataFormat.Json);
            request.AddJsonBody(ElementId);
            var response = _manufacturingApi.Post(request);

            return ExecutionResult.Next();
        }
    }
}
