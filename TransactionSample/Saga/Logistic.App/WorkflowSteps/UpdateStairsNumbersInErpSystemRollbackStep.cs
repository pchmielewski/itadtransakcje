﻿using RestSharp;
using System;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace Logistic.App.WorkflowSteps
{
    public class UpdateStairsNumbersInErpSystemRollbackStep : StepBody
    {
        private readonly RestClient _erpApi;
        public int ElementId { get; set; }
        public UpdateStairsNumbersInErpSystemRollbackStep()
        {
            _erpApi = new RestClient("http://localhost:49885/api/erp");
        }
        public override ExecutionResult Run(IStepExecutionContext context)
        {
            Console.WriteLine("EXECUTED UpdateStairsNumbersInErpSystemRollbackStep");

            var request = new RestRequest("UpdateStairsNumbersInErpSystemRollback", DataFormat.Json);
            request.AddJsonBody(ElementId);
            var response = _erpApi.Post(request);

            return ExecutionResult.Next();
        }
    }
}
