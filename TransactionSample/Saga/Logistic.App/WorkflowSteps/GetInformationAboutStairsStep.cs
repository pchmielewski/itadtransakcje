﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using Logistics.Dto;
using Newtonsoft.Json;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace Logistic.App.WorkflowSteps
{
    public class GetInformationAboutStairsStep : StepBody
    {
        private readonly RestClient _bimElementsApi;
        public int ElementId { get; set; }
        public GetInformationAboutStairsStep()
        {
            _bimElementsApi = new RestClient("http://localhost:49560/api/BimElements/InformationAboutStairs");
        }

        public override ExecutionResult Run(IStepExecutionContext context)
        {
            Console.WriteLine("EXECUTED GetInformationAboutStairsStep");

            var request = new RestRequest("", DataFormat.Json);
            var response = _bimElementsApi.Get(request);

            var serializedResponse = JsonConvert.DeserializeObject<IList<StairsDto>>(response.Content);
            ElementId = serializedResponse.First().Id;

            return ExecutionResult.Next();
        }
    }
}
