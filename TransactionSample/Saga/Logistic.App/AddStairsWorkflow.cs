﻿using Logistic.App.WorkflowSteps;
using System;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace Logistic.App
{
    public class AddStairsWorkflow : IWorkflow<AddStairsWorkflowData>
    {

        public string Id => "AddStairsWorkflow";
        public int Version => 1;

        public void Build(IWorkflowBuilder<AddStairsWorkflowData> builder)
        {
            builder
              .StartWith<GetInformationAboutStairsStep>()
              .Output(d => d.ElementId, d => d.ElementId)
              .Saga(p=>
                p.StartWith<UpdateNumberOfStairsInFactoryStep>()
                    .Input(d=>d.ElementId, d=>d.ElementId)
                    .OnError(WorkflowErrorHandling.Compensate)
                    .CompensateWith<UpdateNumberOfStairsInFactoryRollbackStep>()
                 .Then<AddStairsToSiteStep>()
                    .Input(d => d.ElementId, d => d.ElementId)
                    .CompensateWith<AddStairsToSiteRollbackStep>(p1=>p1.Input(d => d.ElementId, d => d.ElementId))
                    .OnError(WorkflowErrorHandling.Compensate)
                 .Then<UpdateStairsNumbersInErpSystemStep>()
                    .Input(d => d.ElementId, d => d.ElementId)
                    .CompensateWith<UpdateStairsNumbersInErpSystemRollbackStep>(p1 => p1.Input(d => d.ElementId, d => d.ElementId))
                    .OnError(WorkflowErrorHandling.Compensate)
              );
        }
    }
}
