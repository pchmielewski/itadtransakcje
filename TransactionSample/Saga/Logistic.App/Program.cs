﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Logistics.Dto;
using Newtonsoft.Json;
using RestSharp;
using WorkflowCore.Interface;

namespace Logistic.App
{
    class Program
    {
        static async Task Main(string[] args)
        {

            IServiceCollection services = new ServiceCollection();
            services.AddLogging();
            services.AddWorkflow();

            var servicesProvider = services.BuildServiceProvider();

            var host = servicesProvider.GetService<IWorkflowHost>();
            host.RegisterWorkflow<AddStairsWorkflow, AddStairsWorkflowData>();
            host.Start();
            Console.WriteLine("Hello Logistics!");
            Console.WriteLine("Please press any button to move stairs into site");

            await DisplayObjects(1);
            Console.ReadKey();

            try
            {
                var runner = servicesProvider.GetService<ISyncWorkflowRunner>();
                await runner.RunWorkflowSync("AddStairsWorkflow", 1, new AddStairsWorkflowData{ElementId = 0}, null, TimeSpan.FromSeconds(30));

                Console.WriteLine("");
                Console.WriteLine("Successful operation");
                
                // display values 
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed operation");
            }

            await DisplayObjects(1);
            Console.ReadKey();
        }

        private static async Task DisplayObjects(int elementId)
        {
            var logisticApi = new RestClient($"http://localhost:50099/api/Logistics/GetElementsOnSites/{elementId}");
            var request = new RestRequest($"", DataFormat.Json);
            var response = logisticApi.Get(request);

            var elements = JsonConvert.DeserializeObject<IList<ElementsOnSiteDto>>(response.Content);

            Console.WriteLine();
            Console.WriteLine($"ElementsOnSites: |Id|Date                  |ElementId|");
            foreach (var element in elements)
            {
                Console.WriteLine($"ElementsOnSites: |{element.Id} |{element.Date}|{element.ElementId}|");
            }
            Console.WriteLine();

            var erpApi = new RestClient($"http://localhost:49885/api/erp/GetMaterialsEntity/{elementId}");
            var request1 = new RestRequest($"", DataFormat.Json);
            var response1 = erpApi.Get(request1);

            var materials = JsonConvert.DeserializeObject<IList<MaterialsDto>>(response1.Content);

            Console.WriteLine();
            Console.WriteLine($"Materials: |Id|ElementType|NumberOfElements|");
            foreach (var material in materials)
            {
                Console.WriteLine($"Materials: |{material.Id} |{material.ElementType}     |{material.NumberOfElements}|");
            }
            Console.WriteLine();


            var manufacturingApi = new RestClient($"http://localhost:49909/api/Manufacturing/GetFactoryElements/{elementId}");
            var request2 = new RestRequest($"", DataFormat.Json);
            var response2 = manufacturingApi.Get(request2);

            var factoryElements = JsonConvert.DeserializeObject<IList<FactoryElementsDto>>(response2.Content);
            Console.WriteLine();
            Console.WriteLine($"ElementsOnFactory: |Id|ElementType|ElementId|State  |");
            foreach (var element in factoryElements)
            {
                Console.WriteLine($"ElementsOnFactory: |{element.Id} |{element.ElementType}    |{element.ElementId}|{element.State}|");
            }
            Console.WriteLine();
        }
    }
}
