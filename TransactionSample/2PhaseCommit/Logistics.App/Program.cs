﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using BimElements.DAL;
using ERP.DAL;
using Manufacturing.DAL;

namespace Prescient.Logistics.App
{
    static class Program
    {
        static readonly IBimElementsRepository bimElementsRepository = new BimElementsRepository();
        static readonly IErpRepository erpRepository = new ErpRepository();
        static readonly IManufacturingRepository manufacturingRepository = new ManufacturingRepository();

        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello Logistics!");
            Console.WriteLine("Please press any button to move stairs into site");
            Console.ReadKey();

            var stairs = await bimElementsRepository.GetInformationAboutStairs();

            var firstElement = stairs.First();

            using (var transactionScope = new TransactionScope())
            {
                try
                {
                    manufacturingRepository.UpdateNumberOfStairsInFactory(firstElement.Id);
                    erpRepository.AddStairsToSite(firstElement.Id);
                    erpRepository.UpdateStairsNumbersInErpSystem(firstElement.Id);

                    transactionScope.Complete();
                    Console.WriteLine("Succesfull operation");
                    // display values 
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed operation");
                    throw;
                }
            }
        }
    }
}
