﻿namespace BimElements.Model
{
    public class StairsEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfStairs { get; set; }
        public int NumberOfLevels { get; set; }
        public double Weight { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }

    }
}
