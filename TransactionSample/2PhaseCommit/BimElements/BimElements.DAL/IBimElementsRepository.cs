﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BimElements.Model;

namespace BimElements.DAL
{
    public interface IBimElementsRepository
    {
        Task<IList<StairsEntity>> GetInformationAboutStairs();
    }
}
