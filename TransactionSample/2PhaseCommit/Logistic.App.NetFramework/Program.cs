﻿using System;
using System.Transactions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BimElements.DAL;
using ERP.DAL;
using Manufacturing.DAL;

namespace Logistic.App.NetFramework
{
    static class Program
    {
        public static readonly IBimElementsRepository BimElementsRepository = new BimElementsRepository();
        public static readonly IErpRepository ErpRepository = new ErpRepository();
        public static readonly IManufacturingRepository ManufacturingRepository = new ManufacturingRepository();

        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello Logistics!");
            Console.WriteLine("Please press any button to move stairs into site");
            

            var stairs = await BimElementsRepository.GetInformationAboutStairs();

            var firstElement = stairs?.First();

            await DisplayObjects(firstElement.Id);
            Console.ReadKey();

            using (var transactionScope = new TransactionScope())
            {
                try
                {
                    ManufacturingRepository.UpdateNumberOfStairsInFactory(firstElement.Id);
                    ErpRepository.AddStairsToSite(firstElement.Id);
                    ErpRepository.UpdateStairsNumbersInErpSystem(firstElement.Id);

                    transactionScope.Complete();
                    Console.WriteLine("Successful operation");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed operation");
                }
            }

            await DisplayObjects(firstElement.Id);
            Console.ReadKey();
        }

        private static async Task DisplayObjects(int elementId)
        {
            var elements = await ErpRepository.GetElementsOnSites();
            Console.WriteLine();
            Console.WriteLine($"ElementsOnSites: |Id|Date                  |ElementId|");
            foreach (var element in elements)
            {
                Console.WriteLine($"ElementsOnSites: |{element.Id} |{element.Date}|{element.ElementId}|");
            }
            Console.WriteLine();

            var materials = await ErpRepository.GetMaterialsEntity();
            Console.WriteLine();
            Console.WriteLine($"Materials: |Id|ElementType|NumberOfElements|");
            foreach (var material in materials)
            {
                Console.WriteLine($"Materials: |{material.Id} |{material.ElementType}     |{material.NumberOfElements}|");
            }
            Console.WriteLine();


            var factoryElements = await ManufacturingRepository.GetFactoryElementsEntity(elementId);
            Console.WriteLine();
            Console.WriteLine($"ElementsOnFactory: |Id|ElementType|ElementId|State  |");
            foreach (var element in factoryElements)
            {
                Console.WriteLine($"ElementsOnFactory: |{element.Id} |{element.ElementType}    |{element.ElementId}|{element.State}|");
            }
            Console.WriteLine();
        }
    }
}
