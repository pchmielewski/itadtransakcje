﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Model
{
    public class ElementsOnSiteEntity
    {
        public int Id { get; set; }
        public int ElementId { get; set; }
        public DateTime Date { get; set; }
    }
}
