﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.Model;

namespace ERP.DAL
{
    public interface IErpRepository
    {
        bool AddStairsToSite(int stairsId);
        bool UpdateStairsNumbersInErpSystem(int stairsId);
        Task<IList<ElementsOnSiteEntity>> GetElementsOnSites();
        Task<IList<MaterialsEntity>> GetMaterialsEntity();
    }
}
